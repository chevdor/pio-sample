#!/bin/bash

PORT=${PORT:-/dev/cu.usbserial-00005114B}
# PORT=/dev/cu.SLAB_USBtoUART

ADDRESS=0x00001000
BOOTLOADER=bootloader_dio_40m.bin
PARTITIONS=partitions.bin
BOOTAPP0=boot_app0.bin
FIRMWARE=firmware.bin

echo Will deploy to $PORT
ls $BOOTLOADER $PARTITIONS $BOOTAPP0 $FIRMWARE
read -p "Press Enter to proceed"

python3 /Users/will/projects/esp32/esp-idf/components/esptool_py/esptool/esptool.py \
  --chip esp32  \
  --port $PORT \
  --baud 115200 \
  --before default_reset \
  --after hard_reset \
  write_flash -z \
  --flash_mode dio \
  --flash_freq 40m \
  --flash_size detect \
  0xe000 $BOOTAPP0 \
  0x1000  $BOOTLOADER \
  0x10000 $FIRMWARE \
  0x8000 $PARTITIONS
