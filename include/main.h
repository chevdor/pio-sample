#pragma once

#ifndef LED_BUILTIN
#define LED_BUILTIN 25
#endif

#include <NumberCruncher.h>
#include <NumberCruncher.cpp> // required when using template classes to avoid linking problems
#include <IOSwitcher.h>