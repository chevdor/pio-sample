#pragma once

template<class T>
class NumberCruncher {
public:
    NumberCruncher();
    ~NumberCruncher();
    T add(T, T);
    void set(T);
    T get(void);

private:
    T value;
};
