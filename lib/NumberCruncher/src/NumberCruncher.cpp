#include <NumberCruncher.h>

template <class T>
NumberCruncher<T>::NumberCruncher() {
    value = 42;
}

template <class T>
NumberCruncher<T>::~NumberCruncher() {
    value = 0;
}

template <class T>
void NumberCruncher<T>::set(T val) {
   value = val;
}

template <class T>
T NumberCruncher<T>::get(void) {
    return value;
}