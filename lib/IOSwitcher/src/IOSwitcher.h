#pragma once

#ifndef LED_BUILTIN
#define LED_BUILTIN 25
#endif

class IOSwitcher {
public:
    IOSwitcher(int pin);
    ~IOSwitcher();
    void toggle();
    int get();
    int getPin();

private:
    int pin;
};
