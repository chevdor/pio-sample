#include "IOSwitcher.h"
#include "Arduino.h"

IOSwitcher::IOSwitcher(int _pin) {
    this->pin = _pin;
}

IOSwitcher::~IOSwitcher(){
    pin = 0;
}

void IOSwitcher::toggle() {
    int state = digitalRead(pin);
    digitalWrite(pin, state==HIGH?LOW:HIGH);
}

int IOSwitcher::get() {
    return digitalRead(pin);
}

int IOSwitcher::getPin() {
    return pin;
}