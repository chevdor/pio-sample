#include <Arduino.h>
#include <unity.h>
#include <IOSwitcher.h>

IOSwitcher sw(1);

void setUp(void) {
    sw = IOSwitcher(LED_BUILTIN);
}

void tearDown(void) {

}

void test_ctor() {
  TEST_ASSERT_EQUAL(    sw.getPin(), 25);
}


void test_SWitcher() {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(10);
    sw.toggle();
    delay(10);
    TEST_ASSERT_EQUAL(sw.get(), LOW);
}

void setup() {
    delay(2000);
    pinMode(LED_BUILTIN, OUTPUT);

    UNITY_BEGIN();    // IMPORTANT LINE!
    RUN_TEST(test_ctor);
    RUN_TEST(test_SWitcher);
}

void loop() {
    UNITY_END(); // stop unit testing
}
