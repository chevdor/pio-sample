//#include <Arduino.h>
#include <unity.h>
#include <NumberCruncher.h>
#include <NumberCruncher.cpp>

void test_NumberCruncher() {
    NumberCruncher<int> nc;
    TEST_ASSERT_EQUAL(nc.get(), 42);
}

void setup() {
    // NOTE!!! Wait for >2 secs
    // if board doesn't support software reset via Serial.DTR/RTS
    //delay(2000);

    UNITY_BEGIN();    // IMPORTANT LINE!
    RUN_TEST(test_NumberCruncher);
}

void loop() {
    UNITY_END(); // stop unit testing
}
