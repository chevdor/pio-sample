/**
 * Blink
 *
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */
#include "Arduino.h"
#include "main.h"

NumberCruncher<int> nc;
IOSwitcher switcher(LED_BUILTIN);

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  Serial.println("\nSetup complete");
}

void loop()
{

  // turn the LED on (HIGH is the voltage level)
  digitalWrite(LED_BUILTIN, LOW);
  Serial.print("PIN: ");   Serial.println(switcher.getPin());

  int state = digitalRead(LED_BUILTIN);
  Serial.print("State: ");   Serial.println(state);

  // wait for a second
  delay(1000);

  // turn the LED off by making the voltage LOW
  switcher.toggle();
  state = digitalRead(LED_BUILTIN);
  Serial.print("State: ");   Serial.println(state);

   // wait for a second
  delay(1000);
}